<!DOCTYPE html>
<?php 
include 'function.php';
$loremIpsum = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";

?>
<html>
<head>
    <title>COLLATION</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
    <script src="opentype.min.js"></script>

    <?php
    $lesFontes = "fonts/"
    ?>

</head>

    <body> 

        <div id="tolist"><a href="liste_fontes.html">liste des fontes</a></div>
    
        <section id="single_letter">
            <canvas id="chien" class ="cancan"></canvas>
            <canvas id="poney" class ="cancan"></canvas>
        </section>
        <section id="sentence">
        <div id="text" contenteditable="true">Bonjour</div>
            <canvas id="chien2" class="large"></canvas>
            <canvas id="poney2" class="large"></canvas>

            <div id="paragraph">
            <canvas id="paragraph01" class="para"></canvas>
            <canvas id="paragraph02" class="para"></canvas>
            </div>
        </section>

        
        
        
        

        <section id="lockSize01">
            <span id="changeOne" ></span>
            <span id="rangeT01">
            <input class="custom-slider custom-slider-bullet" type="range" id="sizeType01" name="sizeType01"
                min="0" max="2" value="1" step="0.01">
            </span>
        </section>


        <section id="lockSize02">
            <span id="changeTwo"></span>
            <span id="rangeT02">
            <input class="custom-slider custom-slider-bullet" type="range" id="sizeType02" name="sizeType02"
                    min="0" max="2" value="1" step="0.01">
            </span>
        </section>



        


        <section id="listOne">
        <span id="nameselect01">&nbsp;Standard regular ↓</span><br><br>
        <p><input class="fontSelect" id="file01" title="hello" type="file"></p>
        <?php
            display_name($lesFontes,"listOne");
        ?>
        </section>

        <section id="listTwo">
        <span id="nameselect02">&nbsp;Inter regular ↓</span><br><br>
        <p><input class="fontSelect" id="file02" title="salut" type="file" ></p>
        <?php
            display_name($lesFontes,"listTwo");
        ?>
        </section>

        <a href="index.html"><div id="titre">COLLATION</div></a>

        <section id="modificationMenu">
            <span id="switchDay" class="buttonMenu">● ○</span><!--
            --><span id="switch" class="buttonMenu">a ← a</span><!--
            --><span id="changeMetric" class="buttonMenu">metrics</span><!--
            --><span id="changePoint" class="buttonMenu">points</span><!--
            --><span id="changeContour" class="buttonMenu">contours</span><!--
            --><span id="changeOpacity" class="buttonMenu buttonMenuRight">opacity</span>
        </section>

        

    </body>
    <script src="generalfunction.js"></script>
    <script src="script.js"></script>

</html>
