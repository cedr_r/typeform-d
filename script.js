var key = "a";

/// INTERRUPTEUR

//INTERRUPTEUR SUPPERPOSITION
var switchPos = document.getElementById("switch");
var swipo = true;

//INTERRUPTEUR CHANGEMENT UNIQUE OU NON
var switchien = true;
var switchponey = true;
var changeOne = document.getElementById("changeOne");
var changeTwo = document.getElementById("changeTwo");

// INTERUPT JOURNUIT

var switchDay = document.getElementById("switchDay");
var jour = true;

// Interrupteur metric & point

var metric = true;
var points = true;
var opacity = true;
var contour = false;

var changeMetric = document.getElementById("changeMetric");
var changePoint = document.getElementById("changePoint");
var changeOpacity = document.getElementById("changeOpacity");
var changeContour = document.getElementById("changeContour");

var chargefont01 = false;
var chargefont02 = false;

// recupnom de fonte list

var listFont01 = [];
var listFont02 = [];

function triList() {
  var thedisplayfontes = document.getElementsByClassName("fonts");

  for (var i = 0; i < thedisplayfontes.length; i++) {
    var whatlist = thedisplayfontes[i].dataset.list;
    if (whatlist == "listOne") {
      listFont01.push(thedisplayfontes[i]);
    } else {
      listFont02.push(thedisplayfontes[i]);
    }
  }
}
triList();

//// Changement de fontes
var nameselect01 = document.getElementById("nameselect01");
var nameselect02 = document.getElementById("nameselect02");

listFont01.forEach(function (myMeta) {
  myMeta.addEventListener("click", function () {
    chargefont01 = false;
    defaultFont01 = myMeta.dataset.name;
    nameselect01.innerHTML = "&nbsp;" + myMeta.dataset.display + " ↓";
    displayFonte(
      defaultFont01,
      chien,
      colorOne,
      String(key),
      size,
      size,
      sizeApprocheLeft,
      sizeApprocheHaute01,
      defaultSize01
    );
    displayFonte(
      defaultFont01,
      chien2,
      colorOne,
      contentText,
      sizeX2,
      sizeY2,
      sizeApprocheLeft,
      250,
      defaultSizeSentence
    );
    displayFontePara(
      defaultFont01,
      paragraph01,
      ColorParaFont,
      contentPara,
      size,
      size,
      ApprocheLParaFont,
      ApprocheHParaFont,
      SizeParaFont
    );
  });
});

listFont02.forEach(function (myMeta) {
  myMeta.addEventListener("click", function () {
    chargefont02 = false;
    defaultFont02 = myMeta.dataset.name;
    nameselect02.innerHTML = "&nbsp;" + myMeta.dataset.display + " ↓";
    displayFonte(
      defaultFont02,
      poney,
      colorTwo,
      String(key),
      size,
      size,
      sizeApprocheLeft,
      sizeApprocheHaute01,
      defaultSize02
    );
    displayFonte(
      defaultFont02,
      poney2,
      colorTwo,
      contentText,
      sizeX2,
      sizeY2,
      sizeApprocheLeft,
      250,
      defaultSizeSentence
    );
    displayFontePara(
      defaultFont02,
      paragraph02,
      ColorParaFont,
      contentPara,
      size,
      size,
      ApprocheLParaFont,
      ApprocheHParaFont,
      SizeParaFont
    );
  });
});

/// couleur default

var colorOne = "MediumAquaMarine";
var colorTwo = "blueviolet";

var colorBorder = " darkgrey ";
var colorBorderSwitch = " black ";
var borderWidth = " 2px ";
var colorElseSwitch = " white ";
var secondcolorSwitch = " black ";

/// VAR CANVAS

var chien = document.getElementById("chien");
var poney = document.getElementById("poney");

var chien2 = document.getElementById("chien2");
var poney2 = document.getElementById("poney2");

//

var paragraph01 = document.getElementById("paragraph01");
var paragraph02 = document.getElementById("paragraph02");

var contentText = "Bonjour";

var contentPara = [
  "COLLATION   n.f.   Action de comparer entre eux",
  "des textes, des documents.          Le Petit Larousse,",
  "Larousse, Paris, 1995.",
  "",
  "« Les Clercs de la Vie Commune, aux Pays-Bas,",
  "s’occupaient de la collation des originaux dans les",
  "bibliothèques, et du rétablissement du texte des",
  "manuscrits »          François-René de Chateaubriand,",
  "Génie du Christianisme, Migneret, Paris 1802",
];

/// TAILLE DES CANVAS OU FSIZE EST LA TAILLE DE LA FENETRE

///// Chargement taille Canvas et changement au resize ou FSIZE est la width de la fenetre

var size = 0.35 * Fsize;
var sizeX2 = 0.75 * Fsize;
var sizeY2 = 400;

//// Taille Approche gauche et haute du canvas
var sizeApprocheLeft = 0.06 * Fsize;
var sizeApprocheHaute01 = 0.25 * Fsize;

// Taille des fontes

var initSize = 0.28 * Fsize;

var defaultSize01 = initSize;
var defaultSize02 = initSize;
var defaultSizeSentence = 0.12 * Fsize;

window.addEventListener("resize", function () {
  initSize = 0.28 * Fsize;
  defaultSize01 = initSize * sizeType01.value;
  defaultSize02 = initSize * sizeType02.value;
  defaultSizeSentence = 0.12 * Fsize;
  size = 0.35 * Fsize;
  sizeX2 = 0.75 * Fsize;
  sizeApprocheLeft = 0.05 * Fsize;
  sizeApprocheHaute01 = 0.25 * Fsize;
  displayFonte(
    defaultFont01,
    chien,
    colorOne,
    String(key),
    size,
    size,
    sizeApprocheLeft,
    sizeApprocheHaute01,
    defaultSize01
  );
  displayFonte(
    defaultFont02,
    poney,
    colorTwo,
    String(key),
    size,
    size,
    sizeApprocheLeft,
    sizeApprocheHaute01,
    defaultSize02
  );
  displayFonte(
    defaultFont01,
    chien2,
    colorOne,
    contentText,
    sizeX2,
    sizeY2,
    sizeApprocheLeft,
    250,
    defaultSizeSentence
  );
  displayFonte(
    defaultFont02,
    poney2,
    colorTwo,
    contentText,
    sizeX2,
    sizeY2,
    sizeApprocheLeft,
    250,
    defaultSizeSentence
  );
  displayFontePara(
    defaultFont01,
    paragraph01,
    ColorParaFont,
    contentPara,
    size,
    size,
    ApprocheLParaFont,
    ApprocheHParaFont,
    SizeParaFont
  );
  displayFontePara(
    defaultFont02,
    paragraph02,
    ColorParaFont,
    contentPara,
    size,
    size,
    ApprocheLParaFont,
    ApprocheHParaFont,
    SizeParaFont
  );
  console.log("salut");
});

//////

var defaultFont01 = "Standard regular.otf";
var defaultFont02 = "Inter regular.ttf";

document.addEventListener("keydown", function (event) {
  key = event.key;
  console.log(key);

  if (key.length <= 1 && key != " ") {
    if (switchien == true) {
      displayFonte(
        defaultFont01,
        chien,
        colorOne,
        String(key),
        size,
        size,
        sizeApprocheLeft,
        sizeApprocheHaute01,
        defaultSize01
      );
    }
    if (switchponey == true) {
      displayFonte(
        defaultFont02,
        poney,
        colorTwo,
        String(key),
        size,
        size,
        sizeApprocheLeft,
        sizeApprocheHaute01,
        defaultSize02
      );
    }
  }

  setTimeout(changetext, 5);
});

function changetext() {
  contentText = document.getElementById("text").textContent;
  displayFonte(
    defaultFont01,
    chien2,
    colorOne,
    contentText,
    sizeX2,
    sizeY2,
    sizeApprocheLeft,
    250,
    defaultSizeSentence
  );
  displayFonte(
    defaultFont02,
    poney2,
    colorTwo,
    contentText,
    sizeX2,
    sizeY2,
    sizeApprocheLeft,
    250,
    defaultSizeSentence
  );
}

function reset() {
  if (switchponey == false && switchien == false) {
    switchponey = !switchponey;
    switchien = !switchien;
    chien.style.border = borderWidth + " solid " + colorBorder;
    poney.style.border = borderWidth + " solid " + colorBorder;
    changeOne.style.backgroundColor = "black";
    changeTwo.style.backgroundColor = "black";
  }
}

changeTwo.addEventListener("click", function () {
  switchien = !switchien;
  if (switchien == false) {
    poney.style.border = borderWidth + " solid " + colorBorderSwitch;
    changeTwo.style.backgroundColor = colorElseSwitch;
  } else {
    poney.style.border = borderWidth + " solid " + colorBorder;
    changeTwo.style.backgroundColor = secondcolorSwitch;
  }
  reset();
});

changeOne.addEventListener("click", function () {
  switchponey = !switchponey;
  if (switchponey == false) {
    chien.style.border = borderWidth + " solid " + colorBorderSwitch;
    changeOne.style.backgroundColor = colorElseSwitch;
  } else {
    chien.style.border = borderWidth + " solid " + colorBorder;
    changeOne.style.backgroundColor = secondcolorSwitch;
  }
  reset();
});

function displayFonte(
  fontName,
  canvasName,
  color,
  content,
  canvasX,
  canvasY,
  fontposX,
  fontposY,
  fontSize
) {
  if (chargefont01 == true && (canvasName == chien || canvasName == chien2)) {
    displayFonte2(
      fontName,
      canvasName,
      color,
      content,
      canvasX,
      canvasY,
      fontposX,
      fontposY,
      fontSize
    );
  } else if (
    chargefont02 == true &&
    (canvasName == poney || canvasName == poney2)
  ) {
    displayFonte2(
      fontName,
      canvasName,
      color,
      content,
      canvasX,
      canvasY,
      fontposX,
      fontposY,
      fontSize
    );
  } else {
    opentype.load("fonts/" + fontName, function (err, font) {
      if (err) {
        alert("Font could not be loaded: " + err);
      } else {
        // Now let's display it on a canvas with id "canvas"
        const ctx = canvasName.getContext("2d");
        canvasName.width = canvasX;
        canvasName.height = canvasY;
        ctx.clearRect(0, 0, canvasName.width, canvasName.height);

        if (opacity == true) {
          ctx.globalAlpha = 0.5;
        } else {
          ctx.globalAlpha = 1;
        }
        // Construct a Path object containing the letter shapes of the given text.
        // The other parameters are x, y and fontSize.
        // Note that y is the position of the baseline.
        const path = font.getPath(content, fontposX, fontposY, fontSize);
        path.strokeWidth = 3;
        if (contour == false) {
          path.fill = color;
          path.stroke = "transparent";
        } else {
          path.stroke = color;
          path.fill = "transparent";
        }
        // If you just want to draw the text you can also use font.draw(ctx, text, x, y, fontSize).
        path.draw(ctx);

        if (points == true) {
          font.drawPoints(ctx, content, fontposX, fontposY, fontSize);
        }
        if (metric == true) {
          font.drawMetrics(ctx, content, fontposX, fontposY, fontSize);
        }
      }
    });
  }
}

/// FUNCTION FOR PARAGRAPH

var SizeParaFont = 24;
var ApprocheLParaFont = 10;
var ApprocheHParaFont = 28;
var ColorParaFont = "black";

function displayFontePara(
  fontName,
  canvasName,
  color,
  content,
  canvasX,
  canvasY,
  fontposX,
  fontposY,
  fontSize
) {
  if (chargefont01 == true && canvasName == paragraph01) {
    displayFontePara2(
      fontName,
      canvasName,
      color,
      content,
      canvasX,
      canvasY,
      fontposX,
      fontposY,
      fontSize
    );
  } else if (chargefont02 == true && canvasName == paragraph02) {
    displayFontePara2(
      fontName,
      canvasName,
      color,
      content,
      canvasX,
      canvasY,
      fontposX,
      fontposY,
      fontSize
    );
  } else {
    opentype.load("fonts/" + fontName, function (err, font) {
      if (err) {
        alert("Font could not be loaded: " + err);
      } else {
        // Now let's display it on a canvas with id "canvas"
        const ctx = canvasName.getContext("2d");
        canvasName.width = canvasX;
        canvasName.height = canvasY;
        ctx.clearRect(0, 0, canvasName.width, canvasName.height);
        ctx.globalAlpha = 1;
        // Construct a Path object containing the letter shapes of the given text.
        // The other parameters are x, y and fontSize.
        // Note that y is the position of the baseline.

        for (i = 0; i < content.length; i++) {
          const path = font.getPath(
            content[i],
            fontposX,
            fontposY + (5 + fontSize) * i,
            fontSize
          );
          path.fill = color;
          path.draw(ctx);
        }
      }
    });
  }
}

///////////////////////////////////////////////////////////////////////////////////// Chargement initiale des fontes

function init() {
  displayFonte(
    defaultFont01,
    chien,
    colorOne,
    String(key),
    size,
    size,
    sizeApprocheLeft,
    sizeApprocheHaute01,
    defaultSize01
  );

  displayFonte(
    defaultFont02,
    poney,
    colorTwo,
    String(key),
    size,
    size,
    sizeApprocheLeft,
    sizeApprocheHaute01,
    defaultSize02
  );

  displayFonte(
    defaultFont01,
    chien2,
    colorOne,
    contentText,
    sizeX2,
    sizeY2,
    sizeApprocheLeft,
    250,
    defaultSizeSentence
  );

  displayFonte(
    defaultFont02,
    poney2,
    colorTwo,
    contentText,
    sizeX2,
    sizeY2,
    sizeApprocheLeft,
    250,
    defaultSizeSentence
  );

  displayFontePara(
    defaultFont01,
    paragraph01,
    ColorParaFont,
    contentPara,
    size,
    size,
    ApprocheLParaFont,
    ApprocheHParaFont,
    SizeParaFont
  );

  displayFontePara(
    defaultFont02,
    paragraph02,
    ColorParaFont,
    contentPara,
    size,
    size,
    ApprocheLParaFont,
    ApprocheHParaFont,
    SizeParaFont
  );

  /////////////////:::Chargement des phrases
}
init();

function initCanvas() {
  if (switchien == false) {
    chien.style.border = borderWidth + " solid " + colorBorderSwitch;
    changeOne.style.backgroundColor = colorElseSwitch;
  } else {
    chien.style.border = borderWidth + " solid " + colorBorder;
    changeOne.style.backgroundColor = secondcolorSwitch;
  }

  if (switchponey == false) {
    poney.style.border = borderWidth + " solid " + colorBorderSwitch;
    changeTwo.style.backgroundColor = colorElseSwitch;
  } else {
    poney.style.border = borderWidth + " solid " + colorBorder;
    changeTwo.style.backgroundColor = secondcolorSwitch;
  }
  chien2.style.border = borderWidth + " solid " + colorBorder;
  poney2.style.border = borderWidth + " solid " + colorBorder;
}
initCanvas();

// chagnement de place

switchPos.addEventListener("click", function () {
  console.log("salut");
  if (swipo == true) {
    poney.style.left = "20vw";
  } else {
    poney.style.left = "60vw";
  }
  swipo = !swipo;
});

// Deploiement liste fontes

var selectype01 = document.getElementById("listOne");
var selectype02 = document.getElementById("listTwo");

selectype01.addEventListener("mouseover", function () {
  this.style.height = "auto";
});

selectype01.addEventListener("mouseout", function () {
  this.style.height = "20px";
});

selectype02.addEventListener("mouseover", function () {
  this.style.height = "auto";
});

selectype02.addEventListener("mouseout", function () {
  this.style.height = "20px";
});

// Drop font 2

var ismousedown = true;
var posPoney;
var difference;
poney.addEventListener("mousedown", function () {
  ismousedown = true;
  posPoney = poney.getBoundingClientRect().left;
  difference = posX - posPoney;
  dropletter();
});

function dropletter() {
  if (
    ismousedown == true &&
    poney.getBoundingClientRect().right <= window.innerWidth
  ) {
    poney.style.left = posX - difference + "px";
    requestAnimationFrame(dropletter);
  } else if (poney.getBoundingClientRect().right >= window.innerWidth) {
    poney.style.left = "60vw";
    ismousedown = false;
  } else if (poney.getBoundingClientRect().left <= -100) {
    poney.style.left = "20vw";
    ismousedown = false;
  }
}

document.addEventListener("mouseup", function () {
  ismousedown = false;
});

var titre = document.getElementById("titre");

switchDay.addEventListener("click", function () {
  if (jour == true) {
    titre.style.color = "white";
    document.body.style.backgroundColor = "black";
    colorOne = "white";
    colorTwo = "white";
    colorBorderSwitch = "white";
    secondcolorSwitch = " darkgrey ";
    ColorParaFont = "white";

    init();
    initCanvas();
    jour = !jour;
  } else if (jour == false) {
    titre.style.color = "black";
    document.body.style.backgroundColor = "white";
    colorOne = "MediumAquaMarine";
    colorTwo = "blueviolet";
    colorBorderSwitch = " black ";
    secondcolorSwitch = " black ";
    ColorParaFont = "black";

    init();
    initCanvas();
    jour = !jour;
  }
});

// activer metric ou points;

changeMetric.addEventListener("click", function () {
  metric = !metric;
  init();
});

changePoint.addEventListener("click", function () {
  points = !points;
  init();
});

changeOpacity.addEventListener("click", function () {
  opacity = !opacity;
  init();
});

changeContour.addEventListener("click", function () {
  contour = !contour;
  init();
});

//////// CHANGEMENT TAILLE FONTES

// Id inupt range
var sizeType01 = document.getElementById("sizeType01");
var sizeType02 = document.getElementById("sizeType02");

sizeType01.addEventListener("input", function () {
  defaultSize01 = initSize * sizeType01.value;
  displayFonte(
    defaultFont01,
    chien,
    colorOne,
    String(key),
    size,
    size,
    sizeApprocheLeft,
    sizeApprocheHaute01,
    defaultSize01
  );
});

sizeType02.addEventListener("input", function () {
  defaultSize02 = initSize * sizeType02.value;
  displayFonte(
    defaultFont02,
    poney,
    colorTwo,
    String(key),
    size,
    size,
    sizeApprocheLeft,
    sizeApprocheHaute01,
    defaultSize02
  );
});

function onReadFile01(e) {
  var file = e.target.files[0];
  var reader = new FileReader();
  reader.onload = function (e) {
    try {
      chargefont01 = true;
      defaultFont01 = opentype.parse(e.target.result);
      displayFonte(
        defaultFont01,
        chien,
        colorOne,
        String(key),
        size,
        size,
        sizeApprocheLeft,
        sizeApprocheHaute01,
        defaultSize01
      );
      displayFonte(
        defaultFont01,
        chien2,
        colorOne,
        contentText,
        sizeX2,
        sizeY2,
        sizeApprocheLeft,
        250,
        defaultSizeSentence
      );
      displayFontePara(
        defaultFont01,
        paragraph01,
        ColorParaFont,
        contentPara,
        size,
        size,
        ApprocheLParaFont,
        ApprocheHParaFont,
        SizeParaFont
      );
      showErrorMessage("");
      console.log("salut");
    } catch (err) {
      showErrorMessage(err.toString());
    }
  };
  reader.onerror = function (err) {
    showErrorMessage(err.toString());
  };

  reader.readAsArrayBuffer(file);
}

function onReadFile02(e) {
  var file = e.target.files[0];
  var reader = new FileReader();
  reader.onload = function (e) {
    try {
      console.log(opentype.parse(e.target.result));
      chargefont02 = true;
      defaultFont02 = opentype.parse(e.target.result);
      displayFonte(
        defaultFont02,
        poney,
        colorTwo,
        String(key),
        size,
        size,
        sizeApprocheLeft,
        sizeApprocheHaute01,
        defaultSize02
      );
      displayFonte(
        defaultFont02,
        poney2,
        colorTwo,
        contentText,
        sizeX2,
        sizeY2,
        sizeApprocheLeft,
        250,
        defaultSizeSentence
      );
      displayFontePara(
        defaultFont02,
        paragraph02,
        ColorParaFont,
        contentPara,
        size,
        size,
        ApprocheLParaFont,
        ApprocheHParaFont,
        SizeParaFont
      );
      showErrorMessage("");
      console.log("salut");
    } catch (err) {
      showErrorMessage(err.toString());
    }
  };
  reader.onerror = function (err) {
    showErrorMessage(err.toString());
  };

  reader.readAsArrayBuffer(file);
}

var fileButton01 = document.getElementById("file01");
fileButton01.addEventListener("change", onReadFile01, false);

var fileButton02 = document.getElementById("file02");
fileButton02.addEventListener("change", onReadFile02, false);

function displayFonte2(
  font,
  canvasName,
  color,
  content,
  canvasX,
  canvasY,
  fontposX,
  fontposY,
  fontSize
) {
  // Now let's display it on a canvas with id "canvas"
  const ctx = canvasName.getContext("2d");
  canvasName.width = canvasX;
  canvasName.height = canvasY;
  ctx.clearRect(0, 0, canvasName.width, canvasName.height);

  if (opacity == true) {
    ctx.globalAlpha = 0.5;
  } else {
    ctx.globalAlpha = 1;
  }
  // Construct a Path object containing the letter shapes of the given text.
  // The other parameters are x, y and fontSize.
  // Note that y is the position of the baseline.
  const path = font.getPath(content, fontposX, fontposY, fontSize);
  path.strokeWidth = 3;
  if (contour == false) {
    path.fill = color;
    path.stroke = "transparent";
  } else {
    path.stroke = color;
    path.fill = "transparent";
  }
  // If you just want to draw the text you can also use font.draw(ctx, text, x, y, fontSize).
  path.draw(ctx);

  if (points == true) {
    font.drawPoints(ctx, content, fontposX, fontposY, fontSize);
  }
  if (metric == true) {
    font.drawMetrics(ctx, content, fontposX, fontposY, fontSize);
  }
}

function displayFontePara2(
  font,
  canvasName,
  color,
  content,
  canvasX,
  canvasY,
  fontposX,
  fontposY,
  fontSize
) {
  // Now let's display it on a canvas with id "canvas"
  const ctx = canvasName.getContext("2d");
  canvasName.width = canvasX;
  canvasName.height = canvasY;
  ctx.clearRect(0, 0, canvasName.width, canvasName.height);
  ctx.globalAlpha = 1;

  // Construct a Path object containing the letter shapes of the given text.
  // The other parameters are x, y and fontSize.
  // Note that y is the position of the baseline.
  for (i = 0; i < content.length; i++) {
    const path = font.getPath(
      content[i],
      fontposX,
      fontposY + (5 + fontSize) * i,
      fontSize
    );
    path.fill = color;
    path.stroke = "transparent";
    path.draw(ctx);
  }
}
